package pl.mzap.parser.core;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pl.mzap.parser.core.model.DayEvents;
import pl.mzap.parser.core.model.Event;
import pl.mzap.parser.core.model.EventTime;
import pl.mzap.parser.core.model.WeekEvent;
import pl.mzap.parser.core.model.type.EventDayName;
import pl.mzap.parser.infrastructure.template.TagTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

@RequiredArgsConstructor
@Slf4j
public class WeekEventParserService {

	private final DayEventParserService dayEventParserService;

	WeekEvent parseWeekEvents(Document document) {
		log.info("Requesting parse event");
		Elements tbody = document.select(TagTemplate.TABLE.get());
		Elements rows = tbody.select(TagTemplate.ROW.get());

		List<EventTime> eventTimes = getEventTimes(rows);
		List<DayEvents> events = getWeekEvents(rows, eventTimes);
		String groupName = getGroupName(document);
		LocalDate updateTime = getUpdateTime(document);

		return WeekEvent.builder()
				.dayEvents(events)
				.groupName(groupName)
				.updateTime(updateTime)
				.build();
	}

	private List<DayEvents> getWeekEvents(Elements dayRows, List<EventTime> eventTimes) {
		List<DayEvents> weekEvents = new ArrayList<>();
		dayRows.stream()
				.skip(1)
				.forEach(row -> {
					List<Event> dayEvents = dayEventParserService.getDayEvents(eventTimes, row);
					if (isSecondRowOfDay(row)) {
						weekEvents.get(weekEvents.size() - 1).getEvents().addAll(dayEvents);
					} else {
						DayEvents day = DayEvents.builder()
								.dayName(getNameOfDay(row))
								.events(dayEvents)
								.build();
						weekEvents.add(day);
					}
				});

		return weekEvents;
	}

	private boolean isSecondRowOfDay(Element row) {
		return row.select(TagTemplate.ROW_HEADER.get()).isEmpty();
	}

	private EventDayName getNameOfDay(Element row) {
		String dayName = row.select(TagTemplate.ROW_HEADER.get()).text();
		String parsedDayName = dayName.replace(" ", "");
		String shortName = parsedDayName.isEmpty() ? parsedDayName : parsedDayName.substring(0, 2);
		return EventDayName.getEventDayName(shortName);
	}

	private LocalDate getUpdateTime(Document doc) {
		String address = doc.select(TagTemplate.ADDRESS_TAG.get()).text();
		String[] split = address.split(" - ");
		String date = split[2];
		String[] separated = date.split("-");

		return LocalDate.of(Integer.parseInt(separated[0]), Integer.parseInt(separated[1]), Integer.parseInt(separated[2]));
	}

	private String getGroupName(Document doc) {
		String title = doc.select(TagTemplate.TITLE_TAG.get()).text();
		String[] split = title.split(": ");
		return split[1];
	}

	private List<EventTime> getEventTimes(Elements rows) {
		return Optional.ofNullable(rows.first())
				.map(firstRow -> firstRow.select(TagTemplate.ROW_HEADER.get()))
				.map(Collection::stream)
				.map(eventTimesStream -> eventTimesStream
						.skip(1)
						.map(this::mapEventColspanAttr)
						.toList()
				)
				.orElseGet(() -> {
					log.warn("First row is empty");
					return Collections.emptyList();
				});
	}

	private EventTime mapEventColspanAttr(Element eventTime) {
		EventTime eventTime1 = mapToEventTime(eventTime.text());
		if (eventTime.hasAttr(TagTemplate.COLSPAN_ATTR.get())) {
			Integer colspanAttribute = Integer.parseInt(eventTime.attr(TagTemplate.COLSPAN_ATTR.get()));
			eventTime1.setColspanAttr(colspanAttribute);
		}
		return eventTime1;
	}


	private EventTime mapToEventTime(String eventTime) {
		String[] separatedEventTime = eventTime.split("- ");
		String startTime = separatedEventTime[0];
		String endTime = separatedEventTime[1];
		return EventTime.builder()
				.startTime(LocalTime.parse(startTime))
				.endTime(LocalTime.parse(endTime))
				.build();
	}

}
