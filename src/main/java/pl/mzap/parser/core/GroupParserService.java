package pl.mzap.parser.core;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pl.mzap.parser.core.model.Group;
import pl.mzap.parser.infrastructure.template.TagTemplate;

import java.util.List;
import java.util.Objects;

@Slf4j
public class GroupParserService {

	List<Group> parseGroupList(Document document) {
		log.info("Requesting parse groups");
		Elements groupList = getGroupElements(document);
		return groupList.stream()
				.map(this::mapToGroup)
				.filter(Objects::nonNull)
				.toList();
	}

	private Elements getGroupElements(Document doc) {
		Elements ulTag = doc.select(TagTemplate.UL_TAG.get());
		Elements nobrTags = ulTag.get(1).select(TagTemplate.EVENT_DATA_TAG.get());
		return nobrTags.get(0).select(TagTemplate.LI_TAG.get());
	}

	private Group mapToGroup(Element li) {
		Elements aTag = li.select(TagTemplate.A_TAG.get());
		if (aTag.get(0).hasAttr(TagTemplate.A_HREF_ATTR.get()))
			return Group.builder()
					.groupName(getGroupName(aTag))
					.resourceNumber(getResourceNumber(aTag))
					.build();
		return null;
	}

	private Integer getResourceNumber(Elements aTags) {
		String resourceLink = aTags.get(0).attr(TagTemplate.A_HREF_ATTR.get());
		return parseResourceLink(resourceLink);
	}

	private String getGroupName(Elements aTags) {
		return aTags.get(0).text();
	}

	private Integer parseResourceLink(String resourceLink) {
		String[] separatedLink = resourceLink.split("/");
		String parsedLinkExtension = separatedLink[1].substring(0, separatedLink[1].length() - 5);
		String resourcePrefix = String.valueOf(parsedLinkExtension.charAt(0));
		if (resourcePrefix.equals(TagTemplate.GROUP_PREFIX.get())) {
			String parsedResource = parsedLinkExtension.substring(1);
			return Integer.parseInt(parsedResource);
		}
		return null;
	}

}
