package pl.mzap.parser.core;

import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.springframework.cache.annotation.Cacheable;
import pl.mzap.parser.core.model.Group;
import pl.mzap.parser.core.model.WeekEvent;
import pl.mzap.parser.core.port.incoming.EventParserPort;
import pl.mzap.parser.core.port.incoming.GroupParserPort;
import pl.mzap.parser.core.port.outgoing.DocumentConnector;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@RequiredArgsConstructor
public class ParseFacade implements EventParserPort, GroupParserPort {

	private final DocumentConnector documentConnector;
	private final WeekEventParserService weekEventParserService;
	private final GroupParserService groupParserService;

	@Override
	@Cacheable("events")
	public List<Group> parseGroups() throws IOException, URISyntaxException {
		Document document = documentConnector.getGroupDocument();
		return groupParserService.parseGroupList(document);
	}

	@Override
	@Cacheable("groups")
	public WeekEvent parseEvents(int resource) throws IOException, URISyntaxException {
		Document document = documentConnector.getEventDocumentBy(resource);
		return weekEventParserService.parseWeekEvents(document);
	}

}
