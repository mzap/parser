package pl.mzap.parser.core.model;

import lombok.Builder;
import lombok.Getter;
import pl.mzap.parser.core.model.type.EventDayName;

import java.util.List;

@Builder
@Getter
public class DayEvents {

	private final EventDayName dayName;
	private final List<Event> events;

}
