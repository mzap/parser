package pl.mzap.parser.core.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Group {

	private final String groupName;
	private final Integer resourceNumber;

}
