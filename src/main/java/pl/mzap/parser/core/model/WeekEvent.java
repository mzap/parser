package pl.mzap.parser.core.model;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@Builder
@Getter
public class WeekEvent {

	private final String groupName;
	private final LocalDate updateTime;
	private final List<DayEvents> dayEvents;

}
