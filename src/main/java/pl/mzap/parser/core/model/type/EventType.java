package pl.mzap.parser.core.model.type;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum EventType {

	LECTURE("Wyklad", "wykład", 1),
	LABORATORY("Laboratorium", "laboratorium", 2),
	EXERCISES("Ćwiczenia", "ćwiczenia", 3),
	PROJECT("Projekt", "projekt", 4),
	SEMINAR("Seminarium", "seminarium", 5),
	OTHER("Inne", "inne", -1);

	private final String name;
	private final String shortName;
	private final Integer code;

	public static EventType getEventType(String shortName) {
		return Arrays.stream(EventType.values())
				.filter(eventType -> eventType.shortName.equalsIgnoreCase(shortName))
				.findFirst()
				.orElse(OTHER);
	}

}
