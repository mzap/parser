package pl.mzap.parser.core.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Builder
@Getter
public class EventTime {

	private final LocalTime startTime;
	private final LocalTime endTime;
	@Setter
	private Integer colspanAttr;

}
