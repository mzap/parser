package pl.mzap.parser.core.model.type;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum EventDayName {

	MONDAY("Poniedziałek", "pn", 1),
	THURSDAY("Wtorek", "wt", 2),
	WEDNESDAY("Środa", "śr", 3),
	TUESDAY("Czwartek", "cz", 4),
	FRIDAY("Piątek", "pt", 5),
	NOT_DEFINED("Brak", "br", -1);

	private final String name;
	private final String shortName;
	private final Integer code;

	public static EventDayName getEventDayName(String shortedName) {
		return Arrays.stream(EventDayName.values())
				.filter(eventDayName ->
						(shortedName.equalsIgnoreCase(eventDayName.shortName) ||
								shortedName.equalsIgnoreCase(eventDayName.name.substring(0, 2)))
				)
				.findFirst()
				.orElse(NOT_DEFINED);
	}

}
