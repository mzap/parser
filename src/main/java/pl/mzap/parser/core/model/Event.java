package pl.mzap.parser.core.model;

import lombok.Builder;
import lombok.Getter;
import pl.mzap.parser.core.model.type.EventType;
import pl.mzap.parser.core.model.type.EventWeekType;

import java.util.List;

@Builder
@Getter
public class Event {

	private final EventTime eventTime;
	private final EventWeekType week;
	private final String name;
	private final EventType type;
	private final List<String> localizations;
	private final List<String> lecturers;
	private final List<String> groups;

}
