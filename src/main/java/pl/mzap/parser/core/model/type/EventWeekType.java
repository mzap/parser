package pl.mzap.parser.core.model.type;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum EventWeekType {

	WEEK("Tydzien", "tydzień", 1),
	EVEN("Parzysty", "parzyste", 2),
	ODD("Nieparzysty", "nieparzyste", 3),
	OTHER("Inny", "inny", -1);

	private final String name;
	private final String shortName;
	private final Integer code;

	public static EventWeekType getEventWeekType(String shortName) {
		return Arrays.stream(EventWeekType.values())
				.filter(eventType -> eventType.shortName.equalsIgnoreCase(shortName))
				.findFirst()
				.orElse(WEEK);
	}

}
