package pl.mzap.parser.core;

import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Element;
import pl.mzap.parser.core.model.Event;
import pl.mzap.parser.core.model.EventTime;
import pl.mzap.parser.infrastructure.template.TagTemplate;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@RequiredArgsConstructor
public class DayEventParserService {

	private final EventParserService eventParserService;

	List<Event> getDayEvents(List<EventTime> eventTimes, Element dayRow) {
		List<Event> dayEvents = new ArrayList<>();

		AtomicInteger index = new AtomicInteger();
		dayRow.select(TagTemplate.COL.get()).stream()
				.peek(e -> index.getAndIncrement())
				.filter(this::hasEvent)
				.forEach(dayEvent -> {
					EventTime eventTime = calculatePeriodEventTime(eventTimes, index.get() - 1, dayEvent);
					dayEvent.select(TagTemplate.P_TAG.get())
							.forEach(innerEvent -> {
								Event event = eventParserService.getEvent(eventTime, innerEvent);
								dayEvents.add(event);
							});
				});
		return dayEvents;
	}

	private boolean hasEvent(Element col) {
		return !col.text().isEmpty();
	}

	private EventTime calculatePeriodEventTime(List<EventTime> eventTimes, int index, Element col) {
		EventTime eventTime;
		if (columnHasColspanAttribute(col) && eventTimeRowHasNotColspanAttribute(eventTimes, index)) {
			Integer colspan = Integer.parseInt(col.attr(TagTemplate.COLSPAN_ATTR.get()));
			LocalTime startTime = eventTimes.get(index).getStartTime();
			Integer calculatedEndTimeIndex = calculateEndTimeIndex(index, colspan, eventTimes);
			LocalTime endTime = eventTimes.get(calculatedEndTimeIndex).getEndTime();
			eventTime = EventTime.builder()
					.startTime(startTime)
					.endTime(endTime)
					.build();
		} else {
			eventTime = eventTimes.get(index);
		}
		return eventTime;
	}

	private boolean eventTimeRowHasNotColspanAttribute(List<EventTime> eventTimes, int index) {
		return eventTimes.get(index).getColspanAttr() == null;
	}

	private boolean columnHasColspanAttribute(Element col) {
		return col.hasAttr(TagTemplate.COLSPAN_ATTR.get());
	}

	private Integer calculateEndTimeIndex(Integer index, Integer eventColspan, List<EventTime> eventTimes) {
		Integer endTimeColspan = eventTimes.get(index + eventColspan - 1).getColspanAttr();
		return index + eventColspan - Objects.requireNonNullElse(endTimeColspan, 1);
	}

}
