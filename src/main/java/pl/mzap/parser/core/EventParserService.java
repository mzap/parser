package pl.mzap.parser.core;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pl.mzap.parser.core.model.Event;
import pl.mzap.parser.core.model.EventTime;
import pl.mzap.parser.core.model.type.EventType;
import pl.mzap.parser.core.model.type.EventWeekType;
import pl.mzap.parser.infrastructure.template.TagTemplate;

import java.util.ArrayList;
import java.util.List;

public class EventParserService {

	Event getEvent(EventTime eventTime, Element singleEvent) {
		Elements eventData = singleEvent.select(TagTemplate.EVENT_DATA_TAG.get());
		EventWeekType weekType = getWeekType(singleEvent);
		EventType eventType = getEventType(singleEvent);

		List<String> groups = new ArrayList<>();
		List<String> lecturers = new ArrayList<>();
		List<String> localizations = new ArrayList<>();

		eventData.forEach(data -> {
			if (!data.select(TagTemplate.A_TAG.get()).isEmpty()) {
				Elements aTagOfNobr = data.select(TagTemplate.A_TAG.get());
				if (aTagOfNobr.hasAttr(TagTemplate.A_HREF_ATTR.get())) {
					String aHrefAttribute = aTagOfNobr.attr(TagTemplate.A_HREF_ATTR.get());
					if (prefixOfLink(aHrefAttribute).equals(TagTemplate.GROUP_PREFIX.get())) {
						groups.add(data.text());
					} else if (prefixOfLink(aHrefAttribute).equals(TagTemplate.LECTURER_PREFIX.get())) {
						lecturers.add(data.text());
					} else if (prefixOfLink(aHrefAttribute).equals(TagTemplate.LOCALIZATION_PREFIX.get())) {
						localizations.add(data.text());
					}
				}
			} else if (data.text().contains(TagTemplate.FULL_TIME_COURSE.get())) {
				groups.add(data.text());
			}
		});

		return Event.builder()
				.eventTime(eventTime)
				.name(eventData.get(1).text())
				.lecturers(lecturers)
				.localizations(localizations)
				.groups(groups)
				.type(eventType)
				.week(weekType)
				.build();
	}

	private String prefixOfLink(String aHrefAttribute) {
		return String.valueOf(aHrefAttribute.charAt(0));
	}

	private EventWeekType getWeekType(Element singleEvent) {
		Elements a = singleEvent.select(TagTemplate.A_TAG.get());
		String parsedEventWeekType = a.select(TagTemplate.EVENT_DATA_TAG.get()).text();
		return EventWeekType.getEventWeekType(parsedEventWeekType);
	}

	private EventType getEventType(Element singleEvent) {
		Elements a = singleEvent.select(TagTemplate.A_TAG.get());
		a.get(1).select(TagTemplate.EVENT_DATA_TAG.get()).remove();
		return EventType.getEventType(a.get(1).text().split(",")[0]);
	}

}
