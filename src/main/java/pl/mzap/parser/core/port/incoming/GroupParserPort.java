package pl.mzap.parser.core.port.incoming;

import pl.mzap.parser.core.model.Group;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface GroupParserPort {

	List<Group> parseGroups() throws IOException, URISyntaxException;


}
