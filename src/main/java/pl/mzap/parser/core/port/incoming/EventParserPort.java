package pl.mzap.parser.core.port.incoming;

import pl.mzap.parser.core.model.WeekEvent;

import java.io.IOException;
import java.net.URISyntaxException;

public interface EventParserPort {

	WeekEvent parseEvents(int resource) throws IOException, URISyntaxException;

}
