package pl.mzap.parser.core.port.outgoing;

import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URISyntaxException;

public interface DocumentConnector {

	Document getGroupDocument() throws IOException, URISyntaxException;

	Document getEventDocumentBy(int resource) throws IOException, URISyntaxException;

}
