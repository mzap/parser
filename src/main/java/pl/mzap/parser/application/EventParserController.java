package pl.mzap.parser.application;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mzap.parser.core.model.WeekEvent;
import pl.mzap.parser.core.port.incoming.EventParserPort;

import java.io.IOException;
import java.net.URISyntaxException;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("events")
class EventParserController {

	private final EventParserPort eventParserPort;

	@GetMapping("{resource}")
	public WeekEvent parseGroupEvents(@PathVariable int resource) throws IOException, URISyntaxException {
		log.info("Requesting event with resource {}", resource);
		return eventParserPort.parseEvents(resource);
	}

}
