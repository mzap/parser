package pl.mzap.parser.application;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mzap.parser.core.model.Group;
import pl.mzap.parser.core.port.incoming.GroupParserPort;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("groups")
@Slf4j
class GroupParserController {

	private final GroupParserPort groupParserPort;

	@GetMapping
	public List<Group> getAvailableGroups() throws IOException, URISyntaxException {
		log.info("Requesting groups list");
		return groupParserPort.parseGroups();
	}


}
