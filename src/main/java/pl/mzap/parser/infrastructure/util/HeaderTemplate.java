package pl.mzap.parser.infrastructure.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum HeaderTemplate {

	API_KEY("x-api-key"),
	CLIENT_ID("x-client-id");

	@Getter
	private final String value;

}
