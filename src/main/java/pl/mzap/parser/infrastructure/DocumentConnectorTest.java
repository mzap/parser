package pl.mzap.parser.infrastructure;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.mzap.parser.core.port.outgoing.DocumentConnector;
import pl.mzap.parser.infrastructure.exception.DocumentConnectionException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
@Profile("test")
public class DocumentConnectorTest implements DocumentConnector {

	@Override
	public Document getEventDocumentBy(int resource) {
		URL res = getClass().getClassLoader().getResource("g" + resource + ".html");
		return parseDocument(res);
	}

	@Override
	public Document getGroupDocument() {
		URL res = getClass().getClassLoader().getResource("groups.html");
		return parseDocument(res);
	}

	private Document parseDocument(URL resource) {
		String body = readFileContent(resource);
		return Jsoup.parse(body);
	}

	private String readFileContent(URL res) {
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(res.toURI()));
			return new String(encoded, StandardCharsets.UTF_8);
		} catch (IOException | URISyntaxException | NullPointerException e) {
			throw new DocumentConnectionException(res.toString(), e);
		}
	}

}
