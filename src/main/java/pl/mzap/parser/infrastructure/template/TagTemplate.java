package pl.mzap.parser.infrastructure.template;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TagTemplate {

	FULL_TIME_COURSE("Stacjonarne"),
	LOCALIZATION_PREFIX("r"),
	LECTURER_PREFIX("s"),
	GROUP_PREFIX("g"),
	A_TAG("A"),
	P_TAG("p"),
	LI_TAG("li"),
	UL_TAG("ul"),
	EVENT_DATA_TAG("nobr"),
	ADDRESS_TAG("address"),
	TITLE_TAG("title"),
	A_HREF_ATTR("href"),
	COLSPAN_ATTR("colspan"),
	TABLE("tbody"),
	ROW("tr"),
	ROW_HEADER("th"),
	COL("td");

	private final String value;

	public String get() {
		return value;
	}

}
