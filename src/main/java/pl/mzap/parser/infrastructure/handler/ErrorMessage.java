package pl.mzap.parser.infrastructure.handler;

import lombok.Builder;

@Builder
public class ErrorMessage {

	String message;
	String exceptionName;

}
