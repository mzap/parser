package pl.mzap.parser.infrastructure.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.mzap.parser.infrastructure.exception.DocumentConnectionException;

@ControllerAdvice
@Slf4j
public class ErrorHandler {

	@ExceptionHandler(DocumentConnectionException.class)
	public ResponseEntity<ErrorMessage> handleDocumentConnectionException(RuntimeException ex) {

		log.error("ERROR: ", ex);
		ErrorMessage errorResponse = ErrorMessage.builder()
				.exceptionName(ex.getClass().getSimpleName())
				.message(ex.getMessage())
				.build();

		return ResponseEntity
				.status(HttpStatus.BAD_REQUEST)
				.body(errorResponse);
	}

}
