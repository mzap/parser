package pl.mzap.parser.infrastructure;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import pl.mzap.parser.core.port.outgoing.DocumentConnector;
import pl.mzap.parser.infrastructure.exception.DocumentConnectionException;
import pl.mzap.parser.infrastructure.configuration.Env;

import java.io.IOException;

@Service
@Profile("prod")
@RequiredArgsConstructor
@Slf4j
public class DocumentConnectorProd implements DocumentConnector {

	private final Env env;
	private static final String DOT = ".";

	@Override
	public Document getEventDocumentBy(int resource) {
		String resourceLocation = UriComponentsBuilder.newInstance()
				.scheme(env.getScheme())
				.host(env.getHost())
				.path(env.getGroupEventPath() + DOT + env.getResourceFileExtension())
				.buildAndExpand(resource)
				.toUriString();
		return getDocument(resourceLocation);
	}

	@Override
	public Document getGroupDocument() {
		String resourceLocation = UriComponentsBuilder.newInstance()
				.scheme(env.getScheme())
				.host(env.getHost())
				.path(env.getGroupListPath() + DOT + env.getResourceFileExtension())
				.build()
				.toUriString();
		return getDocument(resourceLocation);
	}

	private Document getDocument(String resourceLocation) {
		try {
			log.debug("Resource location {}", resourceLocation);
			return Jsoup.connect(resourceLocation).get();
		} catch (IOException e) {
			throw new DocumentConnectionException(resourceLocation, e);
		}
	}
}
