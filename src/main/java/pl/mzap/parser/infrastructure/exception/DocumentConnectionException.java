package pl.mzap.parser.infrastructure.exception;

public class DocumentConnectionException extends RuntimeException {

	public DocumentConnectionException(String resourceLocation, Throwable cause) {
		super("Can't connect to resource " + resourceLocation, cause);
	}
}
