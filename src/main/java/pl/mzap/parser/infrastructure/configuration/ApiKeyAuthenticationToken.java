package pl.mzap.parser.infrastructure.configuration;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.Transient;

import java.util.Collection;

@Transient
public class ApiKeyAuthenticationToken extends AbstractAuthenticationToken {

	private final String clientId;

	public ApiKeyAuthenticationToken(String clientId, Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.clientId = clientId;
		setAuthenticated(true);
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return clientId;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
