package pl.mzap.parser.infrastructure.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@ConfigurationProperties(prefix = "secure")
@Configuration
@Setter
@Getter
public class ApiSecureEnv {

	private List<Keys> permitApiKeys;

	@Getter
	@Setter
	public static class Keys {
		String clientId;
		String key;
	}

}
