package pl.mzap.parser.infrastructure.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalTime;

@Configuration
public class GsonBuilderConfiguration {

	@Bean
	public Gson getGson() {
		return new GsonBuilder()
				.registerTypeAdapter(LocalTime.class, new LocalTimeTypeAdapter().nullSafe())
				.registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter().nullSafe())
				.create();
	}

}
