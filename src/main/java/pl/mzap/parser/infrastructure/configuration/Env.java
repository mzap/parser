package pl.mzap.parser.infrastructure.configuration;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "config")
@Configuration
@Setter
@Getter
public class Env {

	private int cacheTtl = 0;
	private String scheme = "http";
	@NotNull
	private String host;
	@NotNull
	private String groupEventPath;
	@NotNull
	private String groupListPath;
	private String resourceFileExtension;

}
