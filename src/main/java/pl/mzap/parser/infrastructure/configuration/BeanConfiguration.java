package pl.mzap.parser.infrastructure.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mzap.parser.core.*;
import pl.mzap.parser.core.port.incoming.EventParserPort;
import pl.mzap.parser.core.port.incoming.GroupParserPort;
import pl.mzap.parser.core.port.outgoing.DocumentConnector;

@Configuration
public class BeanConfiguration {

	@Bean
	@Qualifier("eventParserPort")
	public EventParserPort eventParserPort(DocumentConnector documentConnector) {
		return buildParserFacadeInstance(documentConnector);
	}

	@Bean
	@Qualifier("groupParserPort")
	public GroupParserPort groupParserPort(DocumentConnector documentConnector) {
		return buildParserFacadeInstance(documentConnector);
	}

	private ParseFacade buildParserFacadeInstance(DocumentConnector documentConnector) {
		final var eventParserService = new EventParserService();
		final var dayEventParserService = new DayEventParserService(eventParserService);
		final var weekEventParserService = new WeekEventParserService(dayEventParserService);
		final var groupParserService = new GroupParserService();
		return new ParseFacade(documentConnector, weekEventParserService, groupParserService);
	}

}
