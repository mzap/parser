package pl.mzap.parser.infrastructure.configuration;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.util.StopWatch;

@Aspect
@Slf4j
@EnableAspectJAutoProxy
@Configuration
public class PerformanceAspect {

	@Pointcut("within(pl.mzap.parser.controller..*)")
	public void measureController() {
		//pointcut method should have empty body
	}

	@Pointcut("target(pl.mzap.parser.core.port.outgoing.DocumentConnector)")
	public void measureDocumentClass() {
		//pointcut method should have empty body
	}

	@Pointcut("execution(* pl.mzap.parser.core.WeekEventParserService.parseWeekEvents(..))")
	public void measureWeekEventParserService() {
		//pointcut method should have empty body
	}

	@Pointcut("execution(* pl.mzap.parser.core.GroupParserService.parseGroupList(..))")
	public void measureGroupParserService() {
		//pointcut method should have empty body
	}

	@Pointcut("@annotation(pl.mzap.parser.infrastructure.configuration.annotation.MeasurePerformance)")
	public void measureAnnotatedMethods() {
		//pointcut method should have empty body
	}

	@Around("measureController() || measureDocumentClass() || measureWeekEventParserService() || " +
			"measureGroupParserService() || measureAnnotatedMethods()")
	public Object measureExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
		String className = joinPoint.getSignature().getDeclaringType().getSimpleName();
		String methodName = joinPoint.getSignature().getName();

		StopWatch stopWatch = new StopWatch(className + "." + methodName);
		stopWatch.start();
		Object proceed = joinPoint.proceed();
		stopWatch.stop();

		long executeTime = stopWatch.getTotalTimeMillis();

		if (executeTime > 1000) log.warn("Execute {}.{} in {} ms", className, methodName, executeTime);
		else log.debug("Execute {}.{} in {} ms", className, methodName, executeTime);

		return proceed;
	}

}
