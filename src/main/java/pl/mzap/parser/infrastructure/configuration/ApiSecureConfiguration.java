package pl.mzap.parser.infrastructure.configuration;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.mzap.parser.infrastructure.util.HeaderTemplate;

import java.io.IOException;

@RequiredArgsConstructor
public class ApiSecureConfiguration extends OncePerRequestFilter {

	private final ApiSecureEnv permitSecretEnv;

	@Override
	protected void doFilterInternal(
			HttpServletRequest request,
			HttpServletResponse response,
			FilterChain filterChain
	) throws ServletException, IOException {
		final var authorizationToken = request.getHeader(HeaderTemplate.API_KEY.getValue());
		final var clientId = request.getHeader(HeaderTemplate.CLIENT_ID.getValue());

		if (authorizationToken != null && clientId != null) {
			permitSecretEnv.getPermitApiKeys().stream()
					.filter(authConfig ->
							clientId.equals(authConfig.getClientId()) && authorizationToken.equals(authConfig.getKey())
					)
					.findAny()
					.ifPresentOrElse(credential -> {
						final var clientAuthentication = new ApiKeyAuthenticationToken(
								credential.clientId, AuthorityUtils.NO_AUTHORITIES
						);
						SecurityContextHolder.getContext().setAuthentication(clientAuthentication);
					}, () -> response.setStatus(HttpStatus.UNAUTHORIZED.value()));
		} else {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
		}
		filterChain.doFilter(request, response);
	}
}
