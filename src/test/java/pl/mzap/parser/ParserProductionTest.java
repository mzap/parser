package pl.mzap.parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.mzap.parser.core.model.Group;
import pl.mzap.parser.core.model.WeekEvent;
import pl.mzap.parser.core.ParseFacade;
import pl.mzap.parser.core.port.incoming.EventParserPort;
import pl.mzap.parser.core.port.incoming.GroupParserPort;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles(profiles = "prod")
class ParserProductionTest {

	private static final String EMPTY_STRING = "";
	private static final Random random = new Random(System.currentTimeMillis());

	@Autowired
	private GroupParserPort groupParserPort;

	@Autowired
	private EventParserPort eventParserPort;


	@Test
	void groupListReader() throws IOException, URISyntaxException {
		List<Group> groups = getGroups();
		assertNotNull(groups);
		groups
				.forEach(group -> {
					assertNotNull(group.getResourceNumber());
					assertNotEquals(EMPTY_STRING, group.getGroupName());
				});
	}

	@Disabled("Run it if you have exact number with failed resource")
	@ParameterizedTest
	@ValueSource(ints = {7982})
	void groupEventReaderByResourceId(int resourceId) throws IOException, URISyntaxException {
		parseSchedule(resourceId);
	}

	@Test
	void groupEventReader() throws IOException, URISyntaxException {
		Group group = getRandomGroup();
		Integer resource = group.getResourceNumber();
		parseSchedule(resource);
	}

	private void parseSchedule(Integer resourceId) throws IOException, URISyntaxException {
		WeekEvent weekEvent = eventParserPort.parseEvents(resourceId);
		assertNotNull(weekEvent);

		Assertions.assertNotEquals(EMPTY_STRING, weekEvent.getGroupName());
		Assertions.assertNotNull(weekEvent.getUpdateTime());
		weekEvent.getDayEvents()
				.forEach(dayEvent -> {
					Assertions.assertNotNull(dayEvent.getDayName());
					dayEvent.getEvents()
							.forEach(event -> {
								Assertions.assertNotNull(event.getEventTime());
								Assertions.assertNotNull(event.getGroups());
								Assertions.assertNotNull(event.getLecturers());
								Assertions.assertNotNull(event.getLocalizations());
								Assertions.assertNotEquals(EMPTY_STRING, event.getName());
								Assertions.assertNotNull(event.getWeek());
								Assertions.assertNotNull(event.getType());
								Assertions.assertNotEquals(EMPTY_STRING, event.getName());
							});
				});
	}

	private Group getRandomGroup() throws IOException, URISyntaxException {
		List<Group> groups = getGroups();
		return groups.get(getRandomNumber(groups.size()));
	}

	private int getRandomNumber(int groupSize) {
		return random.nextInt(groupSize - 1);
	}

	private List<Group> getGroups() throws IOException, URISyntaxException {
		return groupParserPort.parseGroups();
	}

}
