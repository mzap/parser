package pl.mzap.parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Spy;
import pl.mzap.parser.core.*;
import pl.mzap.parser.core.model.Group;
import pl.mzap.parser.core.model.WeekEvent;
import pl.mzap.parser.infrastructure.DocumentConnectorTest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

class ParserStaticTest {

	private static final String EMPTY_STRING = "";

	@Spy
	private ParseFacade parseFacade;

	@BeforeEach
	public void setUp() {
		this.parseFacade = new ParseFacade(
				new DocumentConnectorTest(),
				new WeekEventParserService(new DayEventParserService(new EventParserService())),
				new GroupParserService()
		);
	}

	@Test
	void groupListReader() throws IOException, URISyntaxException {
		List<Group> groups = getGroups();
		Assertions.assertNotNull(groups);
		groups
				.forEach(group -> {
					Assertions.assertNotNull(group.getResourceNumber());
					Assertions.assertNotEquals(EMPTY_STRING, group.getGroupName());
				});
	}

	@ParameterizedTest
	@ValueSource(ints = {10985, 28140})
	void groupEventReader(int resourceId) throws IOException, URISyntaxException {
		WeekEvent weekEvent = parseFacade.parseEvents(resourceId);
		Assertions.assertNotNull(weekEvent);

		Assertions.assertNotEquals(EMPTY_STRING, weekEvent.getGroupName());
		Assertions.assertNotNull(weekEvent.getUpdateTime());
		weekEvent.getDayEvents()
				.forEach(dayEvent -> {
					Assertions.assertNotNull(dayEvent.getDayName());
					dayEvent.getEvents()
							.forEach(event -> {
								Assertions.assertNotNull(event.getEventTime());
								Assertions.assertNotNull(event.getGroups());
								Assertions.assertNotNull(event.getLecturers());
								Assertions.assertNotNull(event.getLocalizations());
								Assertions.assertNotEquals(EMPTY_STRING, event.getName());
								Assertions.assertNotNull(event.getWeek());
								Assertions.assertNotNull(event.getType());
								Assertions.assertNotEquals(EMPTY_STRING, event.getName());
							});
				});
	}

	private List<Group> getGroups() throws IOException, URISyntaxException {
		return parseFacade.parseGroups();
	}

}
