package pl.mzap.parser.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.mzap.parser.core.ParseFacade;
import pl.mzap.parser.core.model.WeekEvent;
import pl.mzap.parser.core.port.incoming.EventParserPort;
import pl.mzap.parser.core.port.incoming.GroupParserPort;
import pl.mzap.parser.infrastructure.configuration.ApiSecureConfiguration;
import pl.mzap.parser.infrastructure.configuration.ApiSecureEnv;
import pl.mzap.parser.infrastructure.configuration.GsonBuilderConfiguration;
import pl.mzap.parser.infrastructure.handler.ErrorHandler;
import pl.mzap.parser.infrastructure.util.HeaderTemplate;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
class WebLayerTest {

	private MockMvc mockMvc;

	@MockBean
	private ApiSecureEnv apiSecureEnv;

	@Autowired
	private EventParserPort eventParserPort;

	@Autowired
	private GroupParserPort groupParserPort;

	@MockBean
	private ParseFacade parseFacade;

	@BeforeEach
	void setUp() {
		final var mockedPermittedSecret = new ApiSecureEnv.Keys();
		mockedPermittedSecret.setClientId("clientId");
		mockedPermittedSecret.setKey("clientSecret");

		when(apiSecureEnv.getPermitApiKeys())
				.thenReturn(List.of(mockedPermittedSecret));

		this.mockMvc = MockMvcBuilders
				.standaloneSetup(new EventParserController(eventParserPort), new GroupParserController(groupParserPort))
				.setControllerAdvice(new ErrorHandler())
				.setMessageConverters(new GsonHttpMessageConverter(new GsonBuilderConfiguration().getGson()))
				.addFilter(new ApiSecureConfiguration(apiSecureEnv))
				.build();
	}

	@Test
	void securedApiWhenCorrectSecretsTest() throws Exception {
		final var httpHeaders = new HttpHeaders();
		httpHeaders.add(HeaderTemplate.CLIENT_ID.getValue(), "clientId");
		httpHeaders.add(HeaderTemplate.API_KEY.getValue(), "clientSecret");

		when(parseFacade.parseGroups())
				.thenReturn(Collections.emptyList());

		mockMvc.perform(get("/groups").headers(httpHeaders))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	void securedApiWhenIncorrectSecretsTest() throws Exception {
		final var httpHeaders = new HttpHeaders();
		httpHeaders.add(HeaderTemplate.CLIENT_ID.getValue(), "incorrect");
		httpHeaders.add(HeaderTemplate.API_KEY.getValue(), "incorrect");

		when(parseFacade.parseGroups())
				.thenReturn(Collections.emptyList());

		mockMvc.perform(get("/groups").headers(httpHeaders))
				.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	void securedApiWhenEmptySecretsTest() throws Exception {
		when(parseFacade.parseGroups())
				.thenReturn(Collections.emptyList());

		mockMvc.perform(get("/groups"))
				.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	void eventControllerTest() throws Exception {
		final var httpHeaders = new HttpHeaders();
		httpHeaders.add(HeaderTemplate.CLIENT_ID.getValue(), "clientId");
		httpHeaders.add(HeaderTemplate.API_KEY.getValue(), "clientSecret");

		when(parseFacade.parseEvents(0))
				.thenReturn(WeekEvent.builder().build());

		mockMvc.perform(get("/events/0")
								.headers(httpHeaders)
				)
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	void groupControllerTest() throws Exception {
		final var httpHeaders = new HttpHeaders();
		httpHeaders.add(HeaderTemplate.CLIENT_ID.getValue(), "clientId");
		httpHeaders.add(HeaderTemplate.API_KEY.getValue(), "clientSecret");

		when(parseFacade.parseGroups())
				.thenReturn(Collections.emptyList());
		mockMvc.perform(get("/groups").headers(httpHeaders))
				.andDo(print())
				.andExpect(status().isOk());
	}

}
