package pl.mzap.parser.infrastructure;

import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Spy;
import pl.mzap.parser.core.port.outgoing.DocumentConnector;
import pl.mzap.parser.infrastructure.DocumentConnectorTest;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class DocumentReaderTest {

	@Spy
	private DocumentConnector documentConnector;

	@BeforeEach
	public void setUp() {
		this.documentConnector = new DocumentConnectorTest();
	}

	@ParameterizedTest
	@ValueSource(ints = {10985, 28140})
	void eventsDocumentReader(int resourceId) throws IOException, URISyntaxException {
		Document document = documentConnector.getEventDocumentBy(resourceId);
		assertNotNull(document);
	}

	@Test
	void groupListDocumentReader() throws IOException, URISyntaxException {
		Document groupDocument = documentConnector.getGroupDocument();
		assertNotNull(groupDocument);
	}

}
