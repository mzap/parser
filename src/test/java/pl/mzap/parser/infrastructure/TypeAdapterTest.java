package pl.mzap.parser.infrastructure;

import com.google.gson.Gson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.time.LocalTime;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles(profiles = "prod")
class TypeAdapterTest {

	@Autowired
	private Gson gson;

	@Test
	void deserializeLocalDateTypeAdapter() {
		final var inputJson = "\"2022-12-06\"";

		final var localDate = gson.fromJson(inputJson, LocalDate.class);

		Assertions.assertAll(
				() -> Assertions.assertNotNull(localDate),
				() -> Assertions.assertTrue(localDate.isEqual(LocalDate.of(2022, 12, 6)))
		);
	}

	@Test
	void serializeLocalDateTypeAdapter() {
		final var inputDate = LocalDate.of(2022, 12, 6);

		final var localDate = gson.toJson(inputDate, LocalDate.class);

		Assertions.assertAll(
				() -> Assertions.assertNotNull(localDate),
				() -> Assertions.assertEquals("\"2022-12-06\"", localDate)
		);
	}

	@Test
	void deserializeLocalTimeTypeAdapter() {
		final var inputJson = "\"08:00\"";

		final var localTime = gson.fromJson(inputJson, LocalTime.class);

		Assertions.assertAll(
				() -> Assertions.assertNotNull(localTime),
				() -> Assertions.assertEquals(localTime, LocalTime.of(8, 0, 0))
		);
	}

	@Test
	void serializeLocalTimeTypeAdapter() {
		final var inputDate = LocalTime.of(8, 0, 0);

		final var localTime = gson.toJson(inputDate, LocalTime.class);

		Assertions.assertAll(
				() -> Assertions.assertNotNull(localTime),
				() -> Assertions.assertEquals("\"08:00\"", localTime)
		);
	}

}
