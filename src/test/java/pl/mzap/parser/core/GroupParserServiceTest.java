package pl.mzap.parser.core;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import pl.mzap.parser.core.model.Group;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class GroupParserServiceTest {

	@Spy
	GroupParserService groupParserService;

	@BeforeEach
	void setUp() {
		groupParserService = new GroupParserService();
	}

	@Test
	void parseGroupList() throws IOException, URISyntaxException {
		URL res = getClass().getClassLoader().getResource("groups.html");
		Document document = parseDocument(res);

		List<Group> groups = groupParserService.parseGroupList(document);
		Assertions.assertNotNull(groups);
		groups
				.forEach(group -> {
					Assertions.assertNotNull(group.getResourceNumber());
					Assertions.assertNotEquals("", group.getGroupName());
				});
	}

	private Document parseDocument(URL resource) throws IOException, URISyntaxException {
		String body = readFileContent(resource);
		return Jsoup.parse(body);
	}

	private String readFileContent(URL res) throws IOException, URISyntaxException {
		byte[] encoded = Files.readAllBytes(Paths.get(res.toURI()));
		return new String(encoded, StandardCharsets.UTF_8);
	}
}