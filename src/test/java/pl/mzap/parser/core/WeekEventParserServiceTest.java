package pl.mzap.parser.core;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import pl.mzap.parser.core.model.WeekEvent;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class WeekEventParserServiceTest {

	@Spy
	private WeekEventParserService weekEventParserService;

	@BeforeEach
	void setUp() {
		weekEventParserService = new WeekEventParserService(new DayEventParserService(new EventParserService()));
	}

	@ParameterizedTest
	@ValueSource(ints = {10985, 28140})
	void parseWeekEvents(int resourceId) throws IOException, URISyntaxException {
		URL res = getClass().getClassLoader().getResource("g" + resourceId + ".html");
		Document document = parseDocument(res);

		WeekEvent weekEvent = weekEventParserService.parseWeekEvents(document);

		Assertions.assertNotNull(weekEvent);

		Assertions.assertNotEquals("", weekEvent.getGroupName());
		Assertions.assertNotNull(weekEvent.getUpdateTime());
		weekEvent.getDayEvents()
				.forEach(dayEvent -> {
					Assertions.assertNotNull(dayEvent.getDayName());
					dayEvent.getEvents()
							.forEach(event -> {
								Assertions.assertNotNull(event.getEventTime());
								Assertions.assertNotNull(event.getGroups());
								Assertions.assertNotNull(event.getLecturers());
								Assertions.assertNotNull(event.getLocalizations());
								Assertions.assertNotEquals("", event.getName());
								Assertions.assertNotNull(event.getWeek());
								Assertions.assertNotNull(event.getType());
								Assertions.assertNotEquals("", event.getName());
							});
				});
	}

	private Document parseDocument(URL resource) throws IOException, URISyntaxException {
		String body = readFileContent(resource);
		return Jsoup.parse(body);
	}

	private String readFileContent(URL res) throws IOException, URISyntaxException {
		byte[] encoded = Files.readAllBytes(Paths.get(res.toURI()));
		return new String(encoded, StandardCharsets.UTF_8);
	}
}