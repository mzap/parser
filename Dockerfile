FROM maven:3.8.6-eclipse-temurin-17-alpine AS maven-stage

COPY pom.xml /tmp/
COPY src /tmp/src/

WORKDIR /tmp/
RUN mvn package

FROM eclipse-temurin:17

EXPOSE 8095

COPY --from=maven-stage /tmp/target/*.jar /parser.jar
COPY /promtail-linux-amd64 /promtail-linux-amd64
COPY /promtail-config.yml /config.yml
COPY /start_up.sh /start_up.sh

RUN chmod 775 /start_up.sh
RUN chmod 775 /promtail-linux-amd64
RUN chmod 775 /config.yml

ENTRYPOINT ["./start_up.sh"]